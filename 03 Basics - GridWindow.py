#!/usr/bin/env python3

import gi

gi.require_version("Gtk", "4.0")
from gi.repository import Gtk, GLib

"""
* GtkGrid
* GtkBox
* Expand and aligment properties
* GtkInspector - Button6: vexpand, hexpand, then GTK_ALIGN_START. Also, show layout borders.
"""


class GridWindow(Gtk.ApplicationWindow):
    def __init__(self, app):
        super().__init__(title="Grid Example", application=app)

        grid = Gtk.Grid()
        self.set_child(grid)
        # This is fine giving a good first impression, but don't use size constrains
        # for anything else. Let the toolkit reflow as needed.
        self.set_default_size(600, 400)

        button1 = Gtk.Button(label="Button 1")
        button2 = Gtk.Button(label="Button 2")
        button3 = Gtk.Button(label="Button 3")
        button4 = Gtk.Button(label="Button 4")
        button5 = Gtk.Button(label="Button 5")
        button6 = Gtk.Button(label="Button 6")
        # https://www.manpagez.com/html/gtk3/gtk3-3.20.2/ch30s02.php - explanation of valing/xalign and vexpand/hexpand
        # button6.set_hexpand(True)
        # button4.set_halign(Gtk.Align.CENTER)

        grid.attach(button1, 0, 0, 1, 1)
        grid.attach(button2, 1, 0, 2, 1)
        grid.attach_next_to(button3, button1, Gtk.PositionType.BOTTOM, 1, 2)
        grid.attach_next_to(button4, button3, Gtk.PositionType.RIGHT, 2, 1)
        grid.attach(button5, 1, 2, 1, 1)
        grid.attach_next_to(button6, button5, Gtk.PositionType.RIGHT, 1, 1)


def on_activate(app):
    win = GridWindow(app)
    win.present()


app = Gtk.Application()
app.connect("activate", on_activate)
app.run()
