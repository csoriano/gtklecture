#!/usr/bin/env python3

import gi

gi.require_version("Gtk", "4.0")
gi.require_version("WebKit", "6.0")
from gi.repository import Gtk, WebKit

"""
* Container pack end/start
* Lambda functions, convenience needed for non-static functions or functions with
  parameters.
* Linked css class
"""

HOME_PAGE = "http://www.duckduckgo.com"


class BrowserWindow(Gtk.ApplicationWindow):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.set_default_size(750, 600)
        titlebar = Gtk.HeaderBar()
        titlebar.set_show_title_buttons(True)

        self._backbutton = Gtk.Button.new_from_icon_name("go-previous-symbolic")
        self._backbutton.connect("clicked", lambda _: self._webview.go_back())
        self._forwardbutton = Gtk.Button.new_from_icon_name("go-next-symbolic")
        self._forwardbutton.connect("clicked", lambda _: self._webview.go_forward())
        buttonbox = Gtk.Box.new(Gtk.Orientation.HORIZONTAL, 0)
        buttonbox.append(self._backbutton)
        buttonbox.append(self._forwardbutton)
        titlebar.pack_start(buttonbox)
        # FIXME
        #### buttonbox.add_css_class("linked")

        self._urientry = Gtk.Entry()
        self._urientry.set_size_request(300, -1)
        self._urientry.set_text(HOME_PAGE)
        self._urientry.connect("activate", self._urientry_on_activated)
        titlebar.set_title_widget(self._urientry)

        self.set_titlebar(titlebar)
        self._webview = WebKit.WebView()
        self._webview.load_uri(HOME_PAGE)
        self.set_titlebar(titlebar)
        # "notify::" signals names are emitted for every change in a property.
        # This is based on GObject
        self._webview.connect("notify::uri", self._webview_on_uri_changed)
        self.set_child(self._webview)

    def _urientry_on_activated(self, entry):
        uri = self._urientry.get_text()
        self._webview.load_uri(uri)

    def _webview_on_uri_changed(self, signalSender, propertyValue):
        # property value using gi data structures
        self._urientry.set_text(self._webview.get_uri())


class Browser(Gtk.Application):
    def __init__(self):
        super().__init__()
        self._browserwindow = None

    def do_activate(self):
        if self._browserwindow == None:
            self._browserwindow = BrowserWindow(application=self)
        self._browserwindow.present()


app = Browser()
app.run()
