#!/usr/bin/env python3

import gi

gi.require_version("Gtk", "4.0")
from gi.repository import Gtk, GLib

"""
* Buuilder UI design prototype
* GtkBuilder, from file or data
"""

def on_activate(app):
    builder = Gtk.Builder()
    builder.add_from_file("./grid.ui")
    win = builder.get_object("OurWindow")
    app.add_window(win)
    win.present()


app = Gtk.Application()
app.connect("activate", on_activate)
app.run()
