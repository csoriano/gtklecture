# Widgets / Containers
window = createWindow()
grid = createGrid()
button = createButton()
grid.addButton(button)
window.addGrid(grid)

# Events / callbacks
button.connectToClickEvent(onClickEvent)

func onClickEvent():
    print ('Hello world')

# Mainloop
events = Queue() # Shared with other processes and threads

while (true):
    processEvents()

func processEvents():
    event = events.pop()
    event.process()

func addEvent(event):
    events.add(event)
    
# Real main loop code
g_main_context_iterate - gmain.c    3898
g_poll - gpoll.c    124


