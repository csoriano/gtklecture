#!/usr/bin/env python3

import gi

gi.require_version("Gtk", "4.0")
from gi.repository import Gtk, GLib

"""
* Mainloop
* Single-thread. Assumption of memory handling on the main thread.
"""

print("Start")

loop = GLib.MainLoop.new(None, False)

win = Gtk.Window()
win.connect("close-request", lambda x: loop.quit())
win.present()

loop.run()

print("Finish")
