#!/usr/bin/env python3

import gi

gi.require_version("Gtk", "4.0")
from gi.repository import Gtk, Gdk, GLib

"""
* Diferent constructors with GObject
* Icon browser
* Symbolic vs non symbolic
* CSS
* Style providers - Three levels: Toolkit, Application, User.
"""

theme = """
image
{
    animation: spin 1s linear infinite;
}

@keyframes spin
{
    from
    {
        color:red;
    }
    to
    {
        -gtk-icon-transform: rotate(1turn); color: green;
    }
}
"""


class GridWindow(Gtk.ApplicationWindow):
    def __init__(self, app):
        super().__init__(title="Theme", application=app)

        grid = Gtk.Grid()
        self.set_child(grid)

        button1 = Gtk.Button.new_from_icon_name("folder-music-symbolic")
        button2 = Gtk.Button.new_from_icon_name("starred-symbolic")
        button3 = Gtk.Button.new_from_icon_name("weather-few-clouds-symbolic")
        button4 = Gtk.Button.new_from_icon_name("folder-music-symbolic")
        button5 = Gtk.Button.new_from_icon_name("face-kiss-symbolic")
        button6 = Gtk.Button.new_from_icon_name("folder-music-symbolic")
        button6.set_hexpand(True)

        grid.attach(button1, 0, 0, 1, 1)
        grid.attach(button2, 1, 0, 2, 1)
        grid.attach_next_to(button3, button1, Gtk.PositionType.BOTTOM, 1, 2)
        grid.attach_next_to(button4, button3, Gtk.PositionType.RIGHT, 2, 1)
        grid.attach(button5, 1, 2, 1, 1)
        grid.attach_next_to(button6, button5, Gtk.PositionType.RIGHT, 1, 1)

        cssProvider = Gtk.CssProvider()
        # Encoding is necesary because the parameter is a C string, array of bytes,
        # not Unicode
        cssProvider.load_from_data(str.encode(theme))
        Gtk.StyleContext.add_provider_for_display(
            Gdk.Display.get_default(),
            cssProvider,
            Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION,
        )


def on_activate(app):
    win = GridWindow(app)
    win.present()


app = Gtk.Application()
app.connect("activate", on_activate)
app.run()
