#!/usr/bin/env python3

import gi

gi.require_version("Gtk", "4.0")
from gi.repository import Gtk, GLib

"""
* Looking at gtk code - docs: https://amolenaar.github.io/pgi-docgen/
* GObject inheritance
"""


def on_activate(app):
    win = Gtk.ApplicationWindow(application=app)
    win.present()


app = Gtk.Application()
app.connect("activate", on_activate)
app.run()
