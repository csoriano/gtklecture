#!/usr/bin/env python3

import gi

gi.require_version("Gtk", "4.0")
gi.require_version("WebKit", "6.0")
from gi.repository import Gtk, WebKit

"""
* Gtk.Application
* Virtual methods with do_activate
* Header bar
* WebKit and WebKitView as GTK interface
"""

HOME_PAGE = "http://www.duckduckgo.com"


class BrowserWindow(Gtk.ApplicationWindow):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.set_default_size(750, 600)
        titlebar = Gtk.HeaderBar()
        titlebar.set_show_title_buttons(True)

        self.set_titlebar(titlebar)
        self._webview = WebKit.WebView()
        self._webview.load_uri(HOME_PAGE)
        self.set_child(self._webview)


class Browser(Gtk.Application):
    def __init__(self):
        super().__init__()
        self._browserwindow = None

    def do_activate(self):
        if self._browserwindow == None:
            self._browserwindow = BrowserWindow(application=self)
        self._browserwindow.present()


app = Browser()
app.run()
