#!/usr/bin/env python3

import gi

gi.require_version("Gtk", "4.0")
from gi.repository import Gtk, GLib

"""
* Signals/events
* Default handlers for events 

* Python inheritance
* GObject properties and python optional parameters
* Our own event handler
* Layout Manager, Gtk.ApplicationWindow uses a GtkBinLayout as a layout manager which allows setting a
  single child widget
"""


class MyWindow(Gtk.ApplicationWindow):
    def __init__(self, app):
        super().__init__(title="Hello World", application=app)

        button = Gtk.Button(label="Click Here")
        button.connect("clicked", self._on_button_clicked)
        self.set_child(button)

    def _on_button_clicked(self, widget):
        print("Hello World")


def on_activate(app):
    win = MyWindow(app)
    win.present()


app = Gtk.Application()
app.connect("activate", on_activate)
app.run()
